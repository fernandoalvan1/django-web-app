from django import forms
import requests

BASE_URL = "https://countriesnow.space/api/v0.1/countries/capital"


class CapitalForm(forms.Form):
    answer = forms.CharField(label="Answer", max_length=100)

    def search(self, country_data, random_country):
        result = {}
        answer = self.cleaned_data["answer"]
        response = requests.get(BASE_URL)

        if response.status_code == 200:
            for country_info in country_data:
                country_name = country_info["name"]
                country_capital = country_info["capital"]
                if random_country in country_name:
                    if answer is country_capital:
                        result[
                            "message"
                        ] = f"CORRECT - The capital of {random_country} is {country_capital}"
                        result["success"] = True
                    else:
                        result[
                            "message"
                        ] = f"INCORRECT - The capital of {random_country} is {country_capital}"
                        result["success"] = True
            result["success"] = False
        else:
            result["success"] = False
            if response.status_code == 404:
                result["message"] = 'No entry found for "%s"' % answer
            else:
                result[
                    "message"
                ] = "The Countries API is not available at the moment. Please try again later."
        return result
