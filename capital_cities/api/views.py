from django.shortcuts import render
import requests
from .forms import CapitalForm
import random

BASE_URL = "https://countriesnow.space/api/v0.1/countries/capital"


def quiz(request):
    form = CapitalForm()

    response = requests.get(BASE_URL)
    country_data = response.json()["data"]

    random_country = random.choice(country_data)["name"]
    random_country_dict = {"name": random_country}

    search_result = {}
    if "answer" in request.GET:
        random_country = request.GET["random_q"]
        form = CapitalForm(request.GET)
        if form.is_valid():
            search_result = form.search(country_data, random_country)
    return render(
        request,
        "static/quiz.html",
        {
            "form": form,
            "random_country": random_country_dict,
            "search_result": search_result,
        },
    )
