# Django Quiz application

## Setup

The first thing to do is to clone the repository:

```sh
$ git clone https://gitlab.com/fernandoalvan1/django-web-app
$ cd django-web-app
```

Create a virtual environment to install dependencies in and activate it:

```sh
$ virtualenv venv --python python3
$ source venv/bin/activate
```

Then install the dependencies:
```sh
(venv)$ pip install -r requirements.txt
```

Once `pip` has finished downloading the dependencies:
```sh
(env)$ cd capital_cities
(env)$ python manage.py runserver
```

The Country Quiz app should be available at `http://127.0.0.1:8000/quiz/`.